export const state = () => ({
  tilesize: 96,
  viewportCols: 13,
  viewportRows: 8,
  gravity: 8000,
  terminalVelocity: 3200,
  lives: 3,
})

export const mutations = {
  increment (state) {
    state.counter++
  }
}